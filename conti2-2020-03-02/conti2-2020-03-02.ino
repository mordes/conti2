#define ANALOG_PORT   A0
#define WAITING_TIME  100
#define LED           9
#define TARGET        600
#define NUMBER_OF_SAMPLES 16
#define NUMERATOR 100 // Kp = NUMERATOR / DENOMINATOR
#define DENOMINATOR 400

int led_control_value = 255;

void setup() {
  Serial.begin(9600);
  analogReference(DEFAULT);
  pinMode(LED, OUTPUT);
  analogWrite(LED, led_control_value);
}

void loop() {
  unsigned int adc_value = 0;
  int k;
  int i;
  
  for (i = 0; i < NUMBER_OF_SAMPLES; i++) {
    delay(5);
    adc_value += analogRead(ANALOG_PORT);  
  }
  adc_value /= NUMBER_OF_SAMPLES;
    
  if (TARGET > adc_value) {
    k = (TARGET - adc_value); // e(t) = |y0 - y(t)| - error signal
    k = k * NUMERATOR;
    k = k / DENOMINATOR; // u(t) = e(t) * Kp = (e(t) * NUMERATOR) / DENOMINATOR

    if (k == 0) {
      k = 1;
    }
    
    if (led_control_value > k) {
      led_control_value -= k;
    } else {
      led_control_value = 0;
    }
  }
  else if (TARGET < adc_value) {
      k = (adc_value - TARGET); // e(t) = |y0 - y(t)| - error signal
      k = k * NUMERATOR;
      k = k / DENOMINATOR; // u(t) = e(t) * Kp = (e(t) * NUMERATOR) / DENOMINATOR
  
      if (k == 0) {
        k = 1;
      }
      
      if (led_control_value <= 255 - k) {
        led_control_value += k;
      } else {
        led_control_value = 0;
      }
  }
  else {
  }

  analogWrite(LED, led_control_value);

  Serial.print("Raw ADC value = ");
  Serial.println(adc_value, DEC);

  delay(WAITING_TIME);
}
