#define NR_OF_SAMPLES 8
#define BASIC_DELAY_TIME 5
#define WAITING_TIME 500
#define ANALOG_PIN A0
#define LED 13
#define TRUE 1
#define FALSE 0
#define TRESHOLD 20
#define UPPER_TRESHOLD 100

byte is_led_on;

void setup() {
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  is_led_on = FALSE;
}

void loop() {
  unsigned int adc_value;
  unsigned short int idx;
  
  adc_value = 0;
  for (idx = 1; idx <= NR_OF_SAMPLES; idx++)
  {
    delay(BASIC_DELAY_TIME);
    adc_value += analogRead(ANALOG_PIN);
  }
  
  adc_value /= NR_OF_SAMPLES;

  if (adc_value - TRESHOLD < UPPER_TRESHOLD && is_led_on == FALSE) {
    digitalWrite(LED, HIGH);
    is_led_on = TRUE;
  } 
  if (adc_value + TRESHOLD > UPPER_TRESHOLD && is_led_on == TRUE) {
    digitalWrite(LED, LOW);
    is_led_on = FALSE;
  }
  
  Serial.println(adc_value);

  delay(WAITING_TIME);
}
