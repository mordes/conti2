/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW01_P1_Ultrasonic_basic"                                    */
/*****************************************************************************/
/* Circuit: HC-SR04 ultrasonic sensor                                        */
/*****************************************************************************/

/*
General description:
  This application measures the distance of obstacles by the use of the HC-SR04
ultrasonic sensor.
  Before running this application study the manual of the sensor!
*/

#define ECHO       8    //Pin of Arduino that is to be connected to the ECHO pin of the sensor.
#define TRIGGER    9    //Pin of Arduino that is to be connected to the TRIG pin of the sensor.

#define TRUE  1
#define FALSE 0

#define CYCLE_TIME  500    //Measurement periodicity, ms.

#define TRIGGER_PULSE_WIDTH   12  //To launch ultrasonic wave pulses the TRIG pin of the sensor shall
                                  //get a pulse of 10 microseconds width.

#define DISTANCE_DIVIDER  58      //A constant to get the result in centimeters.
#define RANGE_LIMIT       (2*5800)   //The maximum distance is defined to be 200 cm.
//#define RANGE_LIMIT       5800   //The maximum distance is defined to be 100 cm.
//#define RANGE_LIMIT       (3*5800)   //The maximum distance is defined to be 300 cm.

#define PHASE1_ERROR 300
#define PHASE2_ERROR 200

typedef enum
{
  OK,
  ECHO_PIN_LEVEL_HIGH,
  NO_ECHO,
  OUT_OF_RANGE  
} SENSOR_ERROR_t;

/*
 * Function prototype:  void triggerUltraSound(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         The function sets the trigger level high, waits 12 microseconds and after that
 *                      sets the trigger level low again.
 */
void triggerUltraSound(void)
{
  digitalWrite(TRIGGER,HIGH);
  delayMicroseconds(TRIGGER_PULSE_WIDTH);
  digitalWrite(TRIGGER,LOW);
}

SENSOR_ERROR_t phase_1_check(void)
{
  SENSOR_ERROR_t ret = OK;
  
  unsigned long timestamp = micros();

  while (micros() - timestamp <= PHASE1_ERROR)
  {
    if ( digitalRead(ECHO) == HIGH)
    {
      ret = ECHO_PIN_LEVEL_HIGH;
      break;  
    }  
  }
  return ret;
}

SENSOR_ERROR_t phase_2_check(void)
{
  SENSOR_ERROR_t ret = OK;
  
  unsigned long timestamp = micros();

  while (micros() - timestamp <= PHASE2_ERROR)
  {
    if ( digitalRead(ECHO) == HIGH)
    {
      ret = ECHO_PIN_LEVEL_HIGH;
      break;  
    }  
  }
  return ret;
}

SENSOR_ERROR_t phase_3_check(unsigned long *ppulsewidth)
{
  SENSOR_ERROR_t ret = OK;
  
  unsigned long timestamp = micros();
  unsigned long timestamp2;

  while ((timestamp2 = micros()) - timestamp <= RANGE_LIMIT)
  {
    if ( digitalRead(ECHO) == LOW)
    {
      *ppulsewidth = timestamp2 - timestamp;
      break;  
    }  
  }
  return ret;
}



/*
 * Function prototype:  void measureEcho(unsigned long* pvalue, SENSOR_ERROR* perr)
 * Parameter(s):        Address of a variable used for the echo pulse width.
 *                      Address of a variable for the measurement error.
 * Return:              -
 * Other interfaces:    -
 * Description:         The function measures the length of the echo pulse. The length of this
 *                      pulse is in direct proportion to the obstacle distance.
 */
void measureEcho(unsigned long* p_value, byte* p_error, unsigned long time_stamp_phase_1)
{
  unsigned long length_of_echo_pulse = 0;   //Variable to calculate the echo pulse length.
  unsigned long time_stamp;     //Time-stamp.
  byte rising_edge_detected = FALSE;

  *p_error = 7;    //Initial error code (there is an error).
  
  time_stamp = micros();    //Taking the time-stamp.
  
  while ( micros() - time_stamp <= RANGE_LIMIT)    //Before reaching the range limit the echo pulse shall be detected.
  {
    if ( (digitalRead(ECHO) == HIGH) && (rising_edge_detected == FALSE) )   //If the signal level gets HIGH for the first time...
    {
      rising_edge_detected = TRUE;    //The rising edge has been caught.
      time_stamp = micros();          //The time-stamp is taken again for the pulse.
      if (time_stamp - time_stamp_phase_1 < 300) {
        *p_error = 1;
      } else if (time_stamp - time_stamp_phase_1 < 500) {
        *p_error = 2;
      }
    }
    
    if ( (digitalRead(ECHO) == LOW) && (rising_edge_detected == TRUE) )   //The echo pulse comes to its end...
    {    
      length_of_echo_pulse = micros() - time_stamp;     //Calculating the width of the echo pulse.
   
      if (length_of_echo_pulse > 8746) {
        *p_error = 3;
      } else {
        *p_value = length_of_echo_pulse;     //Returning the measured value.
        *p_error = 0;                    //Returning the error code.
      }
      break;                               //Jumping out of the 'while' cycle.
    }
  }
}



/*
 * Function prototype:  void displayData(unsigned long pulse_width, byte measurement_error)
 * Parameter(s):        Pulse width.
 *                      Error code of the measurement.
 * Return:              -
 * Other interfaces:    -
 * Description:         The function dusplays the measured distance.
 */
void displayData(unsigned long pulse_width, byte measurement_error)
{
  unsigned long distance = 0;

  if(measurement_error > 0)    //If there was any error...
  {
    switch(measurement_error) {
      case 1: Serial.println("Phase 1 error"); break;
      case 2: Serial.println("Phase 2 error"); break;
      case 3: Serial.println("Phase 3 error"); break;
      default: Serial.println("Undefined error"); break;
    }
  }
  else
  {
    distance = pulse_width / DISTANCE_DIVIDER;    //Calculating the distance in cm.
    
    Serial.print("Measured pulse width = ");
    Serial.print(pulse_width,DEC);
    Serial.print(" microseconds. Distance = ");
    Serial.print(distance,DEC);
    Serial.print(" cm.\n");
  }
}



/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         Initializing the serial port, the sensor and Arduino's pins.
 */
void setup()
{
  Serial.begin(9600);           //Initializing the serial bus with a baud rate 9600 bits/s.  

  pinMode(ECHO, OUTPUT);        //Initializing the sensor's signal levels.
  digitalWrite(ECHO,LOW);
  pinMode(TRIGGER, OUTPUT);
  digitalWrite(TRIGGER,LOW);
  
  pinMode(ECHO, INPUT);         //After this the Arduino's pin dedicated to ECHO is set as INPUT.
}



/*
 * Function prototype:  void loop()
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         Cyclically measuring the distance with the sensor.
 */
void loop()
{
  unsigned long pulse_width;    //Variable for the pulse width.
  byte measurement_error;
  
  delay(CYCLE_TIME);    //Waiting a certain time between measurements.
  triggerUltraSound();  //Releasing the ultrasonic pulse sequence.

  unsigned long time_stamp_phase_1 = micros();
  
  measureEcho(&pulse_width,&measurement_error, time_stamp_phase_1);    //Accepting the echo pulse and measuring its length.
  displayData(pulse_width,measurement_error);      //Displaying the measurement's results.

}
