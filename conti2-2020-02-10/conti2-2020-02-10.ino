/*
Vh = 340 m/s

t --> s = v / t = 34000 * 10 ^ -6

https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/

*/

#define CYCLE_TIME 500 // Measurement preidocity, ms

#define TRIGGER_PULSE_WIDTH 12 // To launch ultrasonic wave pulses the TRIG pin of the sensor shall
                               // get a pulse of 10 microseconds width.

#define DISTANCE_DIVIDER 58
#define RANGE_LIMIT 5800 // Max is 100 cm, 11600 is 200 cm

#define TRUE 1
#define FALSE 0

#define TRIGGER 9
#define ECHO 2 // UNO accepts 2,3 as ECHO

unsigned long p_value;
byte p_error;
unsigned long time_stamp_falling, time_stamp_rising;

void setup() {
  pinMode(TRIGGER, OUTPUT); // Sets the TRIGGER as an Output
  pinMode(ECHO, INPUT_PULLUP); // inverts the behavior of the INPUT mode, where HIGH means the sensor is off,
                               //and LOW means the sensor is on
  attachInterrupt(digitalPinToInterrupt(ECHO), measureEcho, CHANGE);                             
  Serial.begin(9600); // Starts the serial communication
}

void triggerUltraSound() {
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(TRIGGER, HIGH);
  delay(TRIGGER_PULSE_WIDTH);
  digitalWrite(TRIGGER, LOW);
}

void measureEcho() {

  p_error = TRUE; // Initial error code (there is an error)

  if ( digitalRead(ECHO) == HIGH ) {
    time_stamp_rising = micros();  
  }

  if ( digitalRead(ECHO) == LOW) {
    time_stamp_falling = micros();
  }

  if ( (time_stamp_rising > 0) && (time_stamp_falling > 0) ) {
    p_value = time_stamp_falling - time_stamp_rising;
    time_stamp_rising = 0;
    time_stamp_falling = 0;
    p_error = FALSE;  
  }
  
}

/*
void measureEcho(unsigned long* p_value, byte* p_error) {
  unsigned long length_of_echo_pulse = 0;
  unsigned long time_stamp;
  byte rising_edge_detected = FALSE;
  
  *p_error = TRUE; // Initial error code (there is an error)

  time_stamp = micros();

  while ( micros() - time_stamp <= RANGE_LIMIT ) {
    if ( (digitalRead(ECHO) == HIGH) && (rising_edge_detected == FALSE) ) {
      rising_edge_detected = TRUE;
      time_stamp = micros();  
    }  
    if ( (digitalRead(ECHO) == LOW) && (rising_edge_detected == TRUE) ) {
      length_of_echo_pulse = micros() - time_stamp;
      *p_value = length_of_echo_pulse;
      *p_error = FALSE;
      break;  
    }
  }
}
*/


void displayData(unsigned long pulse_width, byte measurement_error) {
  unsigned long distance;
  
  if (measurement_error == TRUE) {
    Serial.print("Measurement error. Obstacle too far.\n");  
  } else {
    distance = pulse_width / DISTANCE_DIVIDER;

    Serial.print("Measured pulse width = ");
    Serial.print(pulse_width, DEC);
    Serial.print(" microseconds. Distance = ");
    Serial.print(distance, DEC);
    Serial.print(" cm\n");
  }
}

void loop() {
  unsigned long pulse_width;
  byte measurement_error;

   // Clears the TRIGGER and ECHO
  digitalWrite(TRIGGER, LOW);
  digitalWrite(ECHO, LOW);
  delay(CYCLE_TIME);
  triggerUltraSound();
  //measureEcho(&pulse_width, &measurement_error);
  //displayData(pulse_width, measurement_error);
  displayData(p_value, p_error);
  
}
