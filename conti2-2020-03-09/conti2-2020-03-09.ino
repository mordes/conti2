#define ANALOG_PORT       A0
#define NUMBER_OF_SAMPLES 400
#define WAITING_TIME      50
#define LED               9
#define TARGET            600
#define COEFF_A           24    // Kp1 = COEFF_A / DENOMINATOR
#define COEFF_B           -12   // Kp2 = COEFF_B / DENOMINATOR
                                // if set to 0 -> elsodrendu rsz
#define DENOMINATOR       128

unsigned int m1, m2;
unsigned char control_value;

void setup(void) {
  Serial.begin(9600);
  analogReference(DEFAULT);
  pinMode(LED, OUTPUT);

  m1 = measure_avg();
}

unsigned int measure_avg(void) {
  unsigned int i;
  unsigned long cumulated_adc_values = 0;
    
  for (i = 0; i < NUMBER_OF_SAMPLES; i++) {
    cumulated_adc_values += analogRead(ANALOG_PORT);
  }
  cumulated_adc_values /= NUMBER_OF_SAMPLES;
  
  return (unsigned int)cumulated_adc_values;
}

void loop(void) {
  long int k;
  
  delay(WAITING_TIME);

  m2 = m1;
  m1 = measure_avg();

  Serial.println(m1, DEC);

  k = (TARGET - m2) * COEFF_A + (TARGET - m1) * COEFF_B; // masodrendu rsz
  k /= DENOMINATOR;

  if (k == 0) {
    if (TARGET > m1) {
      k = 1;
    } else if (TARGET < m1) {
      k = -1;
    } else {}
  }
  control_value += k;

  analogWrite(LED, 255 - control_value);
  
}
