#include <AFMotor.h> 

#define TRIGGER                      9
#define ECHO                        10
#define WHEEL_SENSOR                 2
#define BLUE_LED                    13
#define RED_LED                     11
#define ANALOG_INPUT                A0

#define TRUE                         1
#define FALSE                        0

#define INITIALIZING_DELAY          10
#define TRIGGER_PULSE_WIDTH         10
#define MUTE_TIMEOUT               360
#define NO_ECHO_TIMEOUT            200
#define RANGE_LIMIT_TIMEOUT       8700
#define BASIC_DELAY_TIME             1

#define PERIOD_1_HZ                 40
#define PERIOD_2_HZ                 20
#define PERIOD_4_HZ                 10
#define PERIOD_ULTRASONIC          200
#define PERIOD_WHEEL_SENSOR_CHECK  500
#define PERIOD_INFRALED            500

#define BREAKING_DISTANCE           30
#define CLEARANCE                   40

#define DISTANCE_DIVIDER            58
#define NR_OF_SAMPLES               10
#define EDGE_NUMBERS_360            84

AF_DCMotor left(3, MOTOR34_1KHZ); 
AF_DCMotor right(4, MOTOR34_1KHZ);

byte accelerating = FALSE;
byte breaking = FALSE;
byte turning = FALSE;
byte led_red_4hz_on = FALSE;
byte led_red_2hz_on = FALSE;
byte led_blue_1hz_on = FALSE;
byte led_blue_4hz_on = FALSE;

unsigned long time_stamp_infraled;
unsigned long time_stamp_blue_1_hz_led;
unsigned long time_stamp_blue_4_hz_led;
unsigned long time_stamp_red_2_hz_led;
unsigned long time_stamp_red_4_hz_led;
unsigned long time_stamp_ultrasonic;
unsigned long time_stamp_wheel_sensor_check;

unsigned long period_infraled = PERIOD_INFRALED;
unsigned long period_blue_1_hz_led = PERIOD_1_HZ + 50;
unsigned long period_blue_4_hz_led = PERIOD_4_HZ + 100;
unsigned long period_red_2_hz_led = PERIOD_2_HZ + 150;
unsigned long period_red_4_hz_led = PERIOD_4_HZ + 300;
unsigned long period_ultrasonic = PERIOD_ULTRASONIC + 400;
unsigned long period_wheel_sensor_check = PERIOD_WHEEL_SENSOR_CHECK + 600;

volatile int edge_counter = 0;
volatile int prev_edge = -1;

byte motor_speed = 0;
int max_infra_adc_value = 1000000;
int infra_turn_value = 0;

typedef enum {  OK = 0,
                ECHO_PIN_LEVEL_HIGH,
                NO_ECHO,
                OUT_OF_RANGE
              } SENSOR_ERROR;

void ISR_countEdges(void)
{
  edge_counter++;
}

void displayTime(unsigned long l_time)
{
  unsigned int fraction_part;
  unsigned long integer_part;

  fraction_part = (unsigned int)(l_time % 100);
  integer_part = l_time / 100;

  Serial.print(integer_part,DEC);
  Serial.print(".");
  if (fraction_part < 10)   //For the decimal padding, e.g. 3.05. Here for the "0".
  {
    Serial.print("0");
  }
  Serial.print(fraction_part,DEC);  
}
void triggerUltraSound(void)
{
  digitalWrite(TRIGGER,HIGH);
  delayMicroseconds(TRIGGER_PULSE_WIDTH);
  digitalWrite(TRIGGER,LOW);
}

void adjustMotor(int R_value, int L_value)
{   
  if (R_value < 0)
  {
    right.setSpeed((byte)(-R_value));
    right.run(FORWARD); 
  }
  else if (R_value > 0)
  {
    right.setSpeed((byte)(R_value));
    right.run(BACKWARD);
  }
  else
  {
    right.setSpeed((byte)(R_value));
    right.run(RELEASE);
  }

  if (L_value < 0)
  {
    left.setSpeed((byte)(-L_value));
    left.run(FORWARD);
  }
  else if (L_value > 0)
  {
    left.setSpeed((byte)(L_value));
    left.run(BACKWARD);
  }
  else
  {
    left.setSpeed((byte)(L_value));
    left.run(RELEASE);
  }
}

byte phase_1(void)
{
  byte error = FALSE;
  unsigned long timestamp = micros();

  while (micros() - timestamp <= MUTE_TIMEOUT)
  {
    if (digitalRead(ECHO) == HIGH)
    {
      error = TRUE;
      break;
    }
  }
  
  return error;
}

byte phase_2(void)
{
  byte error = TRUE;
  unsigned long timestamp = micros();

  while (micros() - timestamp <= NO_ECHO_TIMEOUT)
  {
    if (digitalRead(ECHO) == HIGH)
    {
      error = FALSE;
      break;
    }
  }
  
  return error;
}

byte phase_3(unsigned long* ppulse_width)
{
  byte error = TRUE;
  unsigned long timestamp = micros();

  while (micros() - timestamp <= RANGE_LIMIT_TIMEOUT)
  {
    if (digitalRead(ECHO) == LOW)
    {
      error = FALSE;
      *ppulse_width = micros() - timestamp;
      break;
    }
  }
  
  return error;
}

void measureEcho(unsigned long* ppw, unsigned int* pdist, SENSOR_ERROR* perr)
{
  SENSOR_ERROR error_code = OK;
  unsigned long pulse_width = RANGE_LIMIT_TIMEOUT;

  if (phase_1() == TRUE)
  {
    error_code = ECHO_PIN_LEVEL_HIGH;
  }
  else if (phase_2() == TRUE)
  {
    error_code = NO_ECHO;
  }
  else if (phase_3(&pulse_width) == TRUE)
  {
    error_code = OUT_OF_RANGE;
  }

  *perr = error_code;
  *ppw = pulse_width;
  *pdist = (unsigned int)((pulse_width + DISTANCE_DIVIDER/2) / DISTANCE_DIVIDER);
}

void distanceMeasurement(void)
{
  unsigned long pulse_width;
  SENSOR_ERROR error_code;
  unsigned int distance;
  
  triggerUltraSound();
  measureEcho(&pulse_width,&distance,&error_code);

  if ( (error_code == ECHO_PIN_LEVEL_HIGH) || (error_code == NO_ECHO) )
  {
    accelerating = FALSE;
    breaking = TRUE;

    edge_counter = 0;
    prev_edge = -1;

    led_red_4hz_on = FALSE;
    led_red_2hz_on = TRUE;
    led_blue_1hz_on = FALSE;
    led_blue_4hz_on = FALSE;
  }
  else if ( distance < BREAKING_DISTANCE )
  {
    accelerating = FALSE;
    breaking = TRUE;

    edge_counter = 0;
    prev_edge = -1;

    led_red_4hz_on = FALSE;
    led_red_2hz_on = FALSE;
    led_blue_1hz_on = FALSE;
    led_blue_4hz_on = TRUE;
  }
  else if ( (distance > CLEARANCE ) && (accelerating == FALSE) && (breaking == FALSE) && (turning == FALSE) )
  {
    accelerating = TRUE;
    breaking = FALSE;

    led_red_4hz_on = FALSE;
    led_red_2hz_on = FALSE;
    led_blue_1hz_on = TRUE;
    led_blue_4hz_on = FALSE;
  }
}

void speedControl(void)
{
  if ( (accelerating == TRUE) && (breaking == FALSE) )
  {
    if (motor_speed == 0)
    {
      motor_speed = 110;
    }
    motor_speed += 10;
    if (motor_speed >= 220)
    {
      breaking = TRUE;
    }
  }
  else if ( (accelerating == FALSE) && (breaking == TRUE) )
  {
    if (motor_speed > 60)
    {
      motor_speed -= 60;
    }
    
    if (motor_speed < 80)
    {
      motor_speed = 0;
      breaking = FALSE;
    }
  }
   
  adjustMotor(motor_speed,motor_speed);
}

void turn(void)
{
  turning = TRUE;

  led_red_4hz_on = FALSE;
  led_red_2hz_on = FALSE;
  led_blue_1hz_on = TRUE;
  led_blue_4hz_on = FALSE;
  
  edge_counter = 0;
  adjustMotor(220, -220);
  while (edge_counter <= EDGE_NUMBERS_360)
  {
    unsigned int adc_value = 0;
    byte idx;
  
    for (idx = 1; idx <= NR_OF_SAMPLES; idx++)
    {
      delay(BASIC_DELAY_TIME);
      adc_value += analogRead(ANALOG_INPUT);
    }
    adc_value /= NR_OF_SAMPLES;
  
    if (adc_value > max_infra_adc_value)
    {
      max_infra_adc_value = adc_value;
      infra_turn_value = edge_counter;
    }
  }
  adjustMotor(0, 0);
  edge_counter = 0;
  
  adjustMotor(220, -220);
  while (edge_counter <= infra_turn_value){}
  adjustMotor(0, 0);
  edge_counter = 0;
  
  turning = FALSE;
}

void task_ultrasonic(void)
{
  distanceMeasurement();
  speedControl();
  displayTime(millis()/10);
  Serial.println("    task_ultrasonic");
}

void task_wheel_sensor_check(void)
{
  if (prev_edge == edge_counter)
  {
    accelerating = FALSE;
    breaking = TRUE;

    led_red_4hz_on = TRUE;
    led_red_2hz_on = FALSE;
    led_blue_1hz_on = FALSE;
    led_blue_4hz_on = FALSE;
  }

  displayTime(millis()/10);
  Serial.println("    task_wheel_sensor_check");
  prev_edge = edge_counter;
}

void task_infraled(void)
{
  unsigned int adc_value = 0;
  byte idx;

  for (idx = 1; idx <= NR_OF_SAMPLES; idx++)
  {
    delay(BASIC_DELAY_TIME);
    adc_value += analogRead(ANALOG_INPUT);
  }
  adc_value /= NR_OF_SAMPLES;

  if ((adc_value < max_infra_adc_value) && (turning == FALSE))
  {
    accelerating = FALSE;
    breaking = FALSE;
    turn();
  }
  displayTime(millis()/10);
  Serial.println("    task_infraled");
}

void task_blink_blue_led(void)
{
  displayTime(millis()/10);
  Serial.println("    task_blink_blue_led");
}

void task_blink_blue_1hz_led(void)
{
  if (led_blue_1hz_on == TRUE)
  {
    byte led_status = digitalRead(BLUE_LED);
    digitalWrite(BLUE_LED, !led_status);
  }
  displayTime(millis()/10);
  Serial.println("    task_blink_blue_1hz_led");
}

void task_blink_blue_4hz_led(void)
{
  if (led_blue_4hz_on == TRUE)
  {
    byte led_status = digitalRead(BLUE_LED);
    digitalWrite(BLUE_LED, !led_status);
  }
  displayTime(millis()/10);
  Serial.println("    task_blink_blue_4hz_led");
}

void task_blink_red_2hz_led(void)
{
  if (led_red_2hz_on == TRUE)
  {
    byte led_status = digitalRead(RED_LED);
    digitalWrite(RED_LED, !led_status);
  }
  displayTime(millis()/10);
  Serial.println("    task_blink_red_2hz_led");
}

void task_blink_red_4hz_led(void)
{
  if (led_red_4hz_on == TRUE)
  {
    byte led_status = digitalRead(RED_LED);
    digitalWrite(RED_LED, !led_status);
  }
  displayTime(millis()/10);
  Serial.println("    task_blink_red_4hz_led");
}

void setup() {
  Serial.begin(9600);
  while(!Serial);

  left.run(RELEASE);
  right.run(RELEASE);

  analogReference(DEFAULT);

  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, OUTPUT);
  digitalWrite(ECHO, LOW);
  digitalWrite(TRIGGER, LOW);
  delay(INITIALIZING_DELAY);
  pinMode(ECHO, INPUT);

  pinMode(BLUE_LED, OUTPUT);
  digitalWrite(BLUE_LED, LOW);
  pinMode(RED_LED, OUTPUT);
  digitalWrite(RED_LED, LOW);

  attachInterrupt(digitalPinToInterrupt(WHEEL_SENSOR), ISR_countEdges, RISING);

  time_stamp_blue_1_hz_led = millis();
  time_stamp_blue_4_hz_led = millis();
  time_stamp_blue_1_hz_led = millis();
  time_stamp_red_2_hz_led = millis();
  time_stamp_red_4_hz_led = millis();
  time_stamp_ultrasonic = millis();
  time_stamp_wheel_sensor_check = millis();
  time_stamp_infraled = millis();
}

void loop() {
    displayTime(millis()/10);
    Serial.print("    time_stamp_infraled: ");
    Serial.println(time_stamp_infraled);
    if (millis() - time_stamp_infraled >= period_infraled)
    {
      period_infraled = PERIOD_INFRALED;
      time_stamp_infraled = millis();
      task_infraled();
    }
  
    displayTime(millis()/10);
    Serial.print("    time_stamp_blue_1_hz_led: ");
    Serial.println(time_stamp_blue_1_hz_led);
    if (millis() - time_stamp_blue_1_hz_led >= period_blue_1_hz_led)
    {
      period_blue_1_hz_led = PERIOD_1_HZ;
      task_blink_blue_1hz_led();
    }

    displayTime(millis()/10);
    Serial.print("    time_stamp_blue_4_hz_led: ");
    Serial.println(time_stamp_blue_4_hz_led);
    if(millis() - time_stamp_blue_4_hz_led >= period_blue_4_hz_led)
    {
      period_blue_4_hz_led = PERIOD_4_HZ;
      time_stamp_blue_4_hz_led = millis();
      task_blink_blue_4hz_led();
    }

    displayTime(millis()/10);
    Serial.print("    time_stamp_red_2_hz_led: ");
    Serial.println(time_stamp_red_2_hz_led);
    if (millis() - time_stamp_red_2_hz_led >= period_red_2_hz_led)
    {
      period_red_2_hz_led = PERIOD_2_HZ;
      time_stamp_red_2_hz_led = millis();
      task_blink_red_2hz_led();
    }

    displayTime(millis()/10);
    Serial.print("    time_stamp_red_4_hz_led: ");
    Serial.println(time_stamp_red_4_hz_led);
    if(millis() - time_stamp_red_4_hz_led >= period_red_4_hz_led)
    {
      period_red_4_hz_led = PERIOD_4_HZ;
      time_stamp_red_4_hz_led = millis();
      task_blink_red_4hz_led();
    }

    displayTime(millis()/10);
    Serial.print("    time_stamp_ultrasonic: ");
    Serial.println(time_stamp_ultrasonic);
    if (millis() - time_stamp_ultrasonic > period_ultrasonic)
    {
      period_ultrasonic = PERIOD_ULTRASONIC;
      time_stamp_ultrasonic = millis();
      task_ultrasonic();
    }

    displayTime(millis()/10);
    Serial.print("    time_stamp_wheel_sensor_check: ");
    Serial.println(time_stamp_wheel_sensor_check);
    if (millis() - time_stamp_wheel_sensor_check > period_wheel_sensor_check)
    {
      period_wheel_sensor_check = PERIOD_WHEEL_SENSOR_CHECK;
      time_stamp_wheel_sensor_check = millis();
      task_wheel_sensor_check();
    }
}
