#include <AFMotor.h>

#define TRIGGER                      9
#define ECHO                        10
#define WHEEL_SENSOR                 2
#define BLUE_LED                    13
#define RED_LED                     11
#define ANALOG_INPUT                A0

#define TRUE                         1
#define FALSE                        0

#define INITIALIZING_DELAY          10
#define TRIGGER_PULSE_WIDTH         10
#define MUTE_TIMEOUT               360
#define NO_ECHO_TIMEOUT            200
#define RANGE_LIMIT_TIMEOUT       8700
#define BASIC_DELAY_TIME             1

#define BREAKING_DISTANCE           30
#define CLEARANCE                   40

#define DISTANCE_DIVIDER            58
#define NR_OF_SAMPLES               10
#define EDGE_NUMBERS_360            84

#define INFRA_HYSTERISIS            200
AF_DCMotor left(3, MOTOR34_1KHZ);
AF_DCMotor right(4, MOTOR34_1KHZ);

byte accelerating = FALSE;
byte breaking = FALSE;
byte turning = FALSE;
byte stop_vehicle = FALSE;
byte adjust = FALSE;
byte initial_run = TRUE;

// led flagek
byte RED_LED_4HZ_ON = FALSE;
byte BLUE_LED_4HZ_ON = FALSE;
byte RED_LED_2HZ_ON = FALSE;
byte BLUE_LED_1HZ_ON = FALSE;

// event flagek
byte wheel_not_rotating = FALSE;
byte is_ultra_msr_error = FALSE;
byte is_goal_in_progress = FALSE;
byte is_goal_achieved = FALSE;

byte period_blue_4_hz_led = 2;
byte period_red_4_hz_led = 2;
byte period_red_2_hz_led = 4;
byte period_blue_1_hz_led = 8;

byte current_period_blue_4_hz_led = 0;
byte current_period_red_4_hz_led = 0;
byte current_period_red_2_hz_led = 0;
byte current_period_blue_1_hz_led = 0;





//unsigned long period_ultrasonic = PERIOD_ULTRASONIC + 120;
//unsigned long period_wheel_sensor_check = PERIOD_WHEEL_SENSOR_CHECK + 180;
//unsigned long period_infraled = PERIOD_INFRALED + 235;

unsigned long period_125 = 125 + 25;
unsigned long period_60 = 60;


unsigned long time_stamp_125;
unsigned long time_stamp_60;

unsigned long iteration_counter = 0;

byte period_counter;

byte turn_edge_number = 0;

volatile int edge_counter = 0;
volatile int prev_edge = -1;

byte motor_speed = 0;
unsigned int max_infra_adc_value = 800;
int infra_turn_value = 0;

typedef enum {  OK = 0,
                ECHO_PIN_LEVEL_HIGH,
                NO_ECHO,
                OUT_OF_RANGE
             } SENSOR_ERROR;

void ISR_countEdges(void) {
  edge_counter++;
}


void displayTime(unsigned long l_time) {
  unsigned int fraction_part;
  unsigned long integer_part;

  fraction_part = (unsigned int)(l_time % 100);
  integer_part = l_time / 100;

  //Serial.print(integer_part,DEC);
  //Serial.print(".");
  if (fraction_part < 10)   //For the decimal padding, e.g. 3.05. Here for the "0".
  {
    //Serial.print("0");
  }
  //Serial.print(fraction_part,DEC);
}

void triggerUltraSound(void) {
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(TRIGGER_PULSE_WIDTH);
  digitalWrite(TRIGGER, LOW);
}

void adjustMotor(int R_value, int L_value) {
  if (R_value < 0)
  {
    right.setSpeed((byte)(-R_value));
    right.run(FORWARD);
  }
  else if (R_value > 0)
  {
    right.setSpeed((byte)(R_value));
    right.run(BACKWARD);
  }
  else
  {
    right.setSpeed((byte)(R_value));
    right.run(RELEASE);
  }

  if (L_value < 0)
  {
    left.setSpeed((byte)(-L_value));
    left.run(FORWARD);
  }
  else if (L_value > 0)
  {
    left.setSpeed((byte)(L_value));
    left.run(BACKWARD);
  }
  else
  {
    left.setSpeed((byte)(L_value));
    left.run(RELEASE);
  }
}

byte phase_1(void) {
  byte error = FALSE;
  unsigned long timestamp = micros();

  while (micros() - timestamp <= MUTE_TIMEOUT)
  {
    if (digitalRead(ECHO) == HIGH)
    {
      error = TRUE;
      break;
    }
  }

  return error;
}

byte phase_2(void) {
  byte error = TRUE;
  unsigned long timestamp = micros();

  while (micros() - timestamp <= NO_ECHO_TIMEOUT)
  {
    if (digitalRead(ECHO) == HIGH)
    {
      error = FALSE;
      break;
    }
  }

  return error;
}

byte phase_3(unsigned long* ppulse_width) {
  byte error = TRUE;
  unsigned long timestamp = micros();

  while (micros() - timestamp <= RANGE_LIMIT_TIMEOUT)
  {
    if (digitalRead(ECHO) == LOW)
    {
      error = FALSE;
      *ppulse_width = micros() - timestamp;
      break;
    }
  }

  return error;
}

void measureEcho(unsigned long* ppw, unsigned int* pdist, SENSOR_ERROR* perr) {
  SENSOR_ERROR error_code = OK;
  unsigned long pulse_width = RANGE_LIMIT_TIMEOUT;

  if (phase_1() == TRUE)
  {
    error_code = ECHO_PIN_LEVEL_HIGH;
  }
  else if (phase_2() == TRUE)
  {
    error_code = NO_ECHO;
  }
  else if (phase_3(&pulse_width) == TRUE)
  {
    error_code = OUT_OF_RANGE;
  }

  *perr = error_code;
  *ppw = pulse_width;
  *pdist = (unsigned int)((pulse_width + DISTANCE_DIVIDER / 2) / DISTANCE_DIVIDER);

  displayTime(millis() / 10);
  //Serial.print(" error_code: ");
  //Serial.println(error_code);
}

void distanceMeasurement(void) {
  unsigned long pulse_width;
  SENSOR_ERROR error_code;
  unsigned int distance;

  triggerUltraSound();
  measureEcho(&pulse_width, &distance, &error_code);

  if ( (error_code == ECHO_PIN_LEVEL_HIGH) || (error_code == NO_ECHO) )
  {
    displayTime(millis() / 10);
    //Serial.print(" ERROR: ");
    //Serial.println(error_code);
    accelerating = FALSE;
    breaking = TRUE;

    edge_counter = 0;
    prev_edge = -1;


    is_ultra_msr_error = TRUE;
    wheel_not_rotating = FALSE;
    is_goal_in_progress = FALSE;
    is_goal_achieved = FALSE;
  }
  else if ( distance < BREAKING_DISTANCE )
  {
    accelerating = FALSE;
    displayTime(millis() / 10);
    //Serial.print(" ERROR: ");
    //Serial.println(error_code);
    breaking = TRUE;

    edge_counter = 0;
    prev_edge = -1;


    is_ultra_msr_error = FALSE;
    wheel_not_rotating = FALSE;
    is_goal_in_progress = FALSE;
    is_goal_achieved = TRUE;
  }
  else if ( (distance > CLEARANCE ) && (accelerating == FALSE) && (breaking == FALSE) && (turning == FALSE) )
  {
    accelerating = TRUE;
    breaking = FALSE;

    is_ultra_msr_error = FALSE;
    wheel_not_rotating = FALSE;
    is_goal_in_progress = TRUE;
    is_goal_achieved = FALSE;
  }

  displayTime(millis() / 10);
  //Serial.print(" distance: ");
  //Serial.println(distance);
}

void speedControl(void) {
  if ( (accelerating == TRUE) && (breaking == FALSE) )
  {
    if (motor_speed == 0)
    {
      motor_speed = 110;
    }
    motor_speed += 10;
    if (motor_speed >= 220)
    {
      breaking = TRUE;
    }
  }
  else if ( (accelerating == FALSE) && (breaking == TRUE) )
  {
    if (motor_speed > 60)
    {
      motor_speed -= 60;
    }

    if (motor_speed < 80)
    {
      motor_speed = 0;
      breaking = FALSE;
    }
  }

  if (turning == FALSE) {
    adjustMotor(motor_speed, motor_speed);
  }
}

void wheel_sensor(void) {
  displayTime(millis() / 10);
  //Serial.println("    wheel_sensor start");

  if ((edge_counter == prev_edge) && (accelerating == TRUE)) {
    accelerating = FALSE;
    breaking = TRUE;
    stop_vehicle = TRUE;

    wheel_not_rotating = TRUE;
    is_ultra_msr_error = FALSE;
    is_goal_in_progress = FALSE;
    is_goal_achieved = FALSE;

  }

  prev_edge = edge_counter;

  displayTime(millis() / 10);
  //Serial.println("    wheel_sensor stop");
}

void ultrasonic(void) {
  displayTime(millis() / 10);
  //Serial.println("    ultrasensor start");

  distanceMeasurement();
  speedControl();

  displayTime(millis() / 10);
  //Serial.println("    ultrasensor end");
}

void infraled(void) {
  displayTime(millis() / 10);
  //Serial.println("    infraled start");

  unsigned int adc_value = 0;
  unsigned int upper_range, lower_range = 0;
  byte idx;

  for (idx = 1; idx <= NR_OF_SAMPLES; idx++)
  {
    delay(BASIC_DELAY_TIME);
    adc_value += analogRead(ANALOG_INPUT);
  }
  adc_value /= NR_OF_SAMPLES;
  displayTime(millis() / 10);

  if ((max_infra_adc_value + INFRA_HYSTERISIS) >= 1023)
    upper_range = 1023;
  else
    upper_range = max_infra_adc_value + INFRA_HYSTERISIS;

  lower_range = max_infra_adc_value - INFRA_HYSTERISIS;

  if (turning == FALSE)
  {
    if ((adc_value > upper_range) || (adc_value < lower_range))
    {
      displayTime(millis() / 10);
      accelerating = FALSE;
      breaking = FALSE;
      turning = TRUE;
      adjust = FALSE;
      Serial.println("adc<max");
      //max_infra_adc_value = -1; //*******/
      edge_counter = 0;
      prev_edge = -1;
      turn_edge_number = EDGE_NUMBERS_360;
    }
    else
    {
      displayTime(millis() / 10);
      turning = FALSE;  ///*******új/
      accelerating = TRUE;
      breaking = FALSE;
      if(adc_value > max_infra_adc_value)
      {
        Serial.println("adc>max");
        max_infra_adc_value = adc_value;
      }
    }
  }
  else {
    if (stop_vehicle == FALSE) {
      displayTime(millis() / 10);

      adjustMotor(220, -220);
      if (edge_counter <= turn_edge_number) {
        if (adc_value > max_infra_adc_value) {
          max_infra_adc_value = adc_value;
          infra_turn_value = edge_counter;

          turning = FALSE;
          accelerating = TRUE;
          breaking = FALSE;
        }
      }
      else {
        if (adjust == TRUE) {
          turning = FALSE;
          adjustMotor(0, 0);
        }
        else {
          turn_edge_number = infra_turn_value;
          edge_counter = 0;
          prev_edge = -1;
          adjust = TRUE;
        }
      }

      displayTime(millis() / 10);
      //Serial.println("    turning end");
    } else {
      accelerating = FALSE;
      breaking = FALSE;
    }
  }

  Serial.print("new meas ");
  Serial.print(adc_value);
  Serial.print(" ");
  Serial.print(max_infra_adc_value);
  Serial.print(" turning: ");
  Serial.print(turning);
  Serial.print("  stop_vehicle: ");
  Serial.print(stop_vehicle);
  Serial.print("  accelerating: ");
  Serial.print(accelerating);
  Serial.print("  breaking: ");
  Serial.println(breaking);
  displayTime(millis() / 10);
  //Serial.println("    infraled end");
}

void movement() {


  displayTime(millis() / 10);
  //Serial.println("    movement task started");

  infraled();
  ultrasonic();
  wheel_sensor();





  displayTime(millis() / 10);
  //Serial.print(" accelerating: ");
  //Serial.println(accelerating);

  displayTime(millis() / 10);
  //Serial.print(" breaking: ");
  //Serial.println(breaking);

  displayTime(millis() / 10);
  //Serial.print(" turning: ");
  //Serial.println(turning);

  displayTime(millis() / 10);
  //Serial.print(" is_current_period_red_4_hz_led: ");
  //Serial.println(current_period_red_4_hz_led);

  displayTime(millis() / 10);
  //Serial.print(" is_current_period_red_2_hz_led: ");
  //Serial.println(current_period_red_2_hz_led);

  displayTime(millis() / 10);
  //Serial.print(" is_current_period_blue_1_hz_led: ");
  //Serial.println(current_period_blue_1_hz_led);

  displayTime(millis() / 10);
  //Serial.print(" is_current_period_blue_4_hz_led: ");
  //Serial.println(current_period_blue_4_hz_led);


  displayTime(millis() / 10);
  //Serial.println("    movement task finished");

  // megvolt-e 360 -> max fényerősség
  // letárolni melyik "foknál" volt az a fényerősség
  // feltételezzük, hogy a 360 után 0-ba állunk vissza, majd elforogni abba a fokba
  // egyenesen megyünk-e
  // ha infraled gyengül, akkor megvolt-e 360 hamis -> minden előről
  // minden iterfációban in infraled, ultrasonic, wheel sensor mérés + előre vagy 360 mozgás
}

void blink_led() {

  // nem forog a kerék, piros 4hz
  // edge_counter == prev_edge && !is_finished
  if (wheel_not_rotating == TRUE) {
    // led kikapcsolása

    BLUE_LED_4HZ_ON = FALSE;
    RED_LED_2HZ_ON = FALSE;
    BLUE_LED_1HZ_ON = FALSE;

    digitalWrite(BLUE_LED, LOW);

    // piros led 4hz
    if (RED_LED_4HZ_ON == TRUE)   //If the LED is ON...
    {
      RED_LED_4HZ_ON = FALSE;   //...it shall be turned OFF.
      digitalWrite(RED_LED, LOW);    //Turning OFF the LED.
      current_period_red_4_hz_led = 1;   //Resetting the period counter to 1.
    }
    else
    {
      current_period_red_4_hz_led++;
      if (current_period_red_4_hz_led >= period_red_4_hz_led)
      {
        RED_LED_4HZ_ON = TRUE;
        digitalWrite(RED_LED, HIGH); // LED ON
      }
    }
  }

  // ultrahangmérésnél hiba adódik, piros 2hz
  else if (is_ultra_msr_error == TRUE) {
    // kék led kikapcsolása
    BLUE_LED_4HZ_ON = FALSE;
    RED_LED_4HZ_ON = FALSE;
    BLUE_LED_1HZ_ON = FALSE;

    digitalWrite(BLUE_LED, LOW);

    // piros led 2hz
    if (RED_LED_2HZ_ON == TRUE)   //If the LED is ON...
    {
      RED_LED_2HZ_ON = FALSE;   //...it shall be turned OFF.
      digitalWrite(RED_LED, LOW);    //Turning OFF the LED.
      current_period_red_2_hz_led = 1;   //Resetting the period counter to 1.
    }
    else
    {
      current_period_red_2_hz_led++;
      if (current_period_red_2_hz_led >= period_red_2_hz_led)
      {
        RED_LED_2HZ_ON = TRUE;
        digitalWrite(RED_LED, HIGH); // LED ON
      }
    }
  }

  // célratartás folyamata, kék 1 hz
  else if (is_goal_in_progress == TRUE) {
    // piros led kikapcsolása
    BLUE_LED_4HZ_ON = FALSE;
    RED_LED_4HZ_ON = FALSE;
    RED_LED_2HZ_ON = FALSE;

    digitalWrite(RED_LED, LOW);

    // kék led 1hz
    if (BLUE_LED_1HZ_ON == TRUE)   //If the LED is ON...
    {
      BLUE_LED_1HZ_ON = FALSE;   //...it shall be turned OFF.
      digitalWrite(BLUE_LED, LOW);    //Turning OFF the LED.
      current_period_blue_1_hz_led = 1;   //Resetting the period counter to 1.
    }
    else
    {
      current_period_blue_1_hz_led++;
      if (current_period_blue_1_hz_led >= period_blue_1_hz_led)
      {
        BLUE_LED_1HZ_ON = TRUE;
        digitalWrite(BLUE_LED, HIGH); // LED ON
      }
    }
  }

  // 30cm-nél közelebb, kék 4hz
  else if (is_goal_achieved == TRUE) {
    // piros led kikapcsolása
    BLUE_LED_1HZ_ON = FALSE;
    RED_LED_4HZ_ON = FALSE;
    RED_LED_2HZ_ON = FALSE;

    digitalWrite(RED_LED, LOW);

    // kék led 4hz
    if (BLUE_LED_4HZ_ON == TRUE)   //If the LED is ON...
    {
      BLUE_LED_4HZ_ON = FALSE;   //...it shall be turned OFF.
      digitalWrite(BLUE_LED, LOW);    //Turning OFF the LED.
      current_period_blue_4_hz_led = 1;   //Resetting the period counter to 1.
    }
    else
    {
      current_period_blue_4_hz_led++;
      if (current_period_blue_4_hz_led >= period_blue_4_hz_led)
      {
        BLUE_LED_4HZ_ON = TRUE;
        digitalWrite(BLUE_LED, HIGH); // LED ON
      }
    }
  }
  else {}
}

void setup() {
  Serial.begin(115200);
  while (!Serial);

  left.run(RELEASE);
  right.run(RELEASE);

  analogReference(DEFAULT);

  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, OUTPUT);
  digitalWrite(ECHO, LOW);
  digitalWrite(TRIGGER, LOW);
  delay(INITIALIZING_DELAY);
  pinMode(ECHO, INPUT);

  pinMode(BLUE_LED, OUTPUT);
  digitalWrite(BLUE_LED, LOW);
  pinMode(RED_LED, OUTPUT);
  digitalWrite(RED_LED, LOW);

  attachInterrupt(digitalPinToInterrupt(WHEEL_SENSOR), ISR_countEdges, RISING);

  time_stamp_60 = millis();
  time_stamp_125 = millis();

}

void loop() {
  //Serial.println();
  iteration_counter++;
  displayTime(millis() / 10);
  //Serial.print(iteration_counter, DEC);
  //Serial.println(" th iteration started");

  /*
    Ha mozgás közben a robotautó nem észleli a kerekek elfordulását, (pl. kicsúszott a
    csatlakozó), a robotautó megáll, a hibát pedig a piros LED periodikus, kb. 4Hz-es
    felvillantásával jelzi.

    Ha az ultrahangos távolságmérésnél hibajel adódik, a robotautó megáll, a hibát pedig a
    piros LED periodikus, kb. 2Hz-es felvillantásával jelzi.

    A robotautó a célra tartás folyamatát (akár egyenes gördülésről, akár helyben történő
    elfordulásról van szó) a kék LED periodikus, kb. 1Hz villog

    Ha a robotautó 30 cm akkor kék 4hz.
  */

  // movement task
  if (millis() - time_stamp_60 >= 60) {
    period_60 = 60;
    time_stamp_60 = millis();
    movement();
  }

  // led task
  if (millis() - time_stamp_125 >= 125) {
    period_125 = 125;
    time_stamp_125 = millis();
    if (initial_run == FALSE) {
      blink_led();  
    }
  }

  initial_run = FALSE;

  displayTime(millis() / 10);
  //Serial.println(iteration_counter, DEC);
  //Serial.print(" th iteration finished");
  //Serial.println();

}
