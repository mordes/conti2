/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW12_P3_Blinking_LED_with_scheduler"                         */
/*****************************************************************************/
/* Circuit: -                                                                */
/*****************************************************************************/
/* Description:                                                              */
/*                                                                           */
/* The program makes a LED blink based on a number received via the USB.     */
/* Range: '1' - '6'.                                                         */
/* The blinking control is hooked to a 32 ms task while the USB monitoring   */
/* is hooked to a 200 ms task.                                               */
/*                                                                           */
/* Blink patterns:                                                           */
/* O...........................................................              */
/* O.............................O.............................              */
/* O...................O...................O...................              */
/* O..............O..............O..............O..............              */
/* O.........O.........O.........O.........O.........O.........              */
/* O.....O.....O.....O.....O.....O.....O.....O.....O.....O.....              */
/*****************************************************************************/

#define ORANGE_LED    9   //Port of the orange LED.

#define FALSE 0
#define TRUE  1

unsigned long time_stamp_32, time_stamp_200;      //Variables used for time stamps of tasks.
unsigned long period_32 = 32+16;    //Period of the 32ms task in ms plus 16ms initial delay.
                                    //Each 800 ms there would be a conflict otherwise. In this way NO conflict!
unsigned long period_200 = 200;     //Period of the 200ms task in ms.

byte period_values[6] = { 60, 30, 20, 15, 10, 6 };   //OFF periods between to consecutive ON periods.
byte LED_on = FALSE;    //A flag indicating whether the LED is turned ON.

byte periods = period_values[0];     //Initial value of the number of OFF periods.
byte period_counter = periods - 1;   //Initial value of the period counter. After startup a blink is made.



/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port, the LED's pin, system clock.
 * Description:         Initialization of the USB, of the LED's pin, of the timestamps of tasks and
 *                      displaying the initial message.
 */
void setup()
{
  Serial.begin(19200);              //Initializing the serial bus with a baud rate 19200 bits/s.
  pinMode(ORANGE_LED, OUTPUT);      //The LED's pin shall be an output pin.
  digitalWrite(ORANGE_LED, HIGH);   //Turning OFF the LED.

  time_stamp_32 = millis();    //Taking the initial time stamps of the tasks.
  time_stamp_200 = millis();

  Serial.println("\n\nEnter a number between 1 and 6!");    //Displaying the initial message.
  Serial.println("Value = 1"); 
}



/*
 * Function prototype:  void Task_32ms(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    The LED's pin, global variables of the blink control.
 * Description:         The 32ms task.
 */
void Task_32ms(void)
{
  if (LED_on == TRUE)   //If the LED is ON...
  {
    LED_on = FALSE;   //...it shall be turned OFF.
    digitalWrite(ORANGE_LED,HIGH);    //Turning OFF the LED.
    period_counter = 1;   //Resetting the period counter to 1.
  }
  else
  {
    period_counter++;   //During the OFF-periods their counter is increased.
    
    if (period_counter >= periods)    //Is the counter bigger than or equal to the prescribed number?
    {
      LED_on = TRUE;    //If yes, the LED shall be turned ON.
      digitalWrite(ORANGE_LED,LOW);   //Turning ON the LED.
    }
  }
}



/*
 * Function prototype:  void Task_200ms(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    USB, global variables of the blink control.
 * Description:         The 200ms task.
 */
void Task_200ms(void)
{
  char incoming_char;   //Variable for the incoming character.
  byte blink_freq;      //Variable to index the blinking frequency.
  
  if (Serial.available()>0)   //Is there a new incoming character?
  {
    incoming_char = Serial.read();    //Reading the incoming character.

    if ( (incoming_char < '1') || (incoming_char > '6') )   //Is it outside range 1 - 6?
    {
      Serial.println("Invalid value");    //Yes, it is.
    }
    else  //The incoming character is valid (1-6)..
    {
      Serial.print("Value = ");   //Displaying it.
      Serial.println(incoming_char);

      blink_freq = (byte)(incoming_char - '1');   //With this conversion the variable will be 0, 1, 2, 3, 4 or 5.

      periods = period_values[blink_freq];    //Taking the number of OFF-periods from their array.
    }
  }

}



/*
 * Function prototype:  void loop(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    System clock.
 * Description:         The SCHEDULER.
 */
void loop(void)
{ 
  /* Evaluating the clock value and calling the appropriate task(s) if conditions are met. */
  
  if (millis() - time_stamp_32 >= 32)
  { 
    period_32 = 32;   //The period of the 32 ms task shall be reset. It is only necessary hence at the
                      //beginning this variable has a bigger value to realize a time shift, to avoid
                      //conflicts betwwen the 2 tasks.
    time_stamp_32 = millis();   //Resetting the time stamp.
    Task_32ms();                //Calling the 32 ms task.
  }

  if (millis() - time_stamp_200 >= period_200)
  { 
    time_stamp_200 = millis();  //Resetting the time stamp.
    Task_200ms();               //Calling the 200 ms task.
  }
}


