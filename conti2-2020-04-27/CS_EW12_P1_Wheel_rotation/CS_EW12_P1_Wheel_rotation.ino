/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW12_P1_Wheel_rotation"                                      */
/*****************************************************************************/
/* Circuit: -                                                                */
/*****************************************************************************/
/* Device: INITIO robot car + motor shield                                   */
/*****************************************************************************/
/* Reference:                                                                */
/* https://learn.adafruit.com/adafruit-motor-shield/af-dcmotor-class         */
/*****************************************************************************/

#include <AFMotor.h>    //Including the library of the motor control shield.

AF_DCMotor left(3, MOTOR34_1KHZ);     //Setting the parameters of the motor of the left side.
AF_DCMotor right(4, MOTOR34_1KHZ);    //Setting the parameters of the motor of the right side.

#define WHEEL_SENSOR        2     //Any signal wire of a wheel sensor can be connected to this pin.
                                  //Note that this pin must be 2 (never used by the shield) or
                                  //3 (can be used by the shield in certain cases. There are 2 external
                                  //interrupts at the Arduino Uno. Pin 2 or 3.
                                  
#define FALSE               0     //Boolean constant.
#define TRUE                1     //Boolean constant.

volatile int edge_counter = 0;    //A variable used for counting the rising edges.
int readout_number = 0;           //Number of readouts of the edge counter.
byte motor_speed = 0;             //Self-explanatory.



/*
 * Function prototype:  void ISR_countEdges(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Global variable 'edge_counter'.
 * Description:         This interrupt service routine (ISR) counts the rising edges of the wheel sensor.
 */
void ISR_countEdges(void)
{
  edge_counter++;
}



/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port, motor control shield.
 * Description:         Initialization of the USB, of the motor shield and attaching the interrupt
 *                      service routine to pin 2.
 */
void setup(void)
{
  Serial.begin(19200);            //Initializing the serial bus with baud rate 19200 bytes/s.
  left.run(RELEASE);              //Stopping the left wheel pair.
  right.run(RELEASE);             //Stopping the right wheel pair.
  
  Serial.println("\n\nTest run started...");    //Displaying the start message.
  Serial.println("Enter speed (0-9)");
  
  attachInterrupt(digitalPinToInterrupt(WHEEL_SENSOR), ISR_countEdges, RISING);   //Attaching the ISR.
  //It means:   pin of the wheel sensor, i.e. pin 2;   function name;   interrupt triggered by rising edges.
}



/*
 * Function prototype:  void loop(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port, motor control shield.
 * Description:         Receiving a number via the USB and setting the motors' speed based on it.
 *                      Informative messages are sent to the USB, for testing purposes.
 */
void loop(void)
{
  char incoming_char;
  
  if (Serial.available()>0)   //Is there a new incoming character?
  {
    incoming_char = Serial.read();    //Reading the incoming character.

    if ( (incoming_char < '0') || (incoming_char > '9') )   //Is it outside range 0 - 9?
    {
      Serial.println("Invalid value");    //Yes, it is.
    }
    else  //The incoming character is valid (0-9)..
    {
      if (incoming_char == '0')   //Is it '0'?
      {
        motor_speed = 0;                //Stopping the motors.
        left.setSpeed(motor_speed);     //Setting the speed of the left wheel pair.
        right.setSpeed(motor_speed);    //Setting the speed of the right wheel pair.
        left.run(RELEASE);              //Stopping the left wheel pair.
        right.run(RELEASE);             //Stopping the right wheel pair.
      }
      else
      {
        edge_counter = 0;       //Resetting the edge counter.
        readout_number = 0;     //Resetting the readout number.
        motor_speed = (byte)(incoming_char - '0') * 15 + 100;   //Converting the character to a byte value.
        left.setSpeed(motor_speed);       //Setting the speed of the left wheel pair.
        right.setSpeed(motor_speed);      //Setting the speed of the right wheel pair.
        left.run(FORWARD);    //Starting the left wheel pair.
        right.run(FORWARD);   //Starting the right wheel pair.      
      }
      
      Serial.print("Motor speed = ");   //Displaying the motor speed.
      Serial.println(motor_speed,DEC);
    }
  }

  delay(200);   //Waiting 200 ms.
  
  if (motor_speed > 0)    //Are the motors running?
  {
    readout_number++;   //Increasing the readout number.
    //Serial.print("Readout: ");    //Displaying the number of readouts.
    //Serial.print(readout_number,DEC);
    //Serial.print("    edge counter = ");  //Displaying the edge counter.
    Serial.println(edge_counter,DEC);
  }
}

