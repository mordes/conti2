/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW12_P3_Infrared_measurement"                                */
/*****************************************************************************/
/* Circuit: Phototransistor_3 - with an anfrared phototransistor             */
/*****************************************************************************/
/* Description:                                                              */
/*                                                                           */
/* The program makes a LED blink based on the irradiance level measured by   */
/* a phototransistor with infrared sensitivity.                              */
/* The blinking control is hooked to a 32 ms task while the measurement is   */
/* hooked to a 500 ms task.                                                  */
/*                                                                           */
/* Blink patterns:                                                           */
/* O...........................................................              */
/* O.............................O.............................              */
/* O...................O...................O...................              */
/* O..............O..............O..............O..............              */
/* O.........O.........O.........O.........O.........O.........              */
/* O.....O.....O.....O.....O.....O.....O.....O.....O.....O.....              */
/*****************************************************************************/

#define ANALOG_INPUT        A0      //Port chosen for AD-conversion.
#define NR_OF_SAMPLES       10      //Number of samples for forming the average value.
                                    //Note that this number can at most be 64!
                                    
#define BASIC_DELAY_TIME     1      //Basic time delay between AD conversions.

#define ORANGE_LED    9   //Port of the orange LED.

#define NUM_OF_FREQUENCIES          6   //There are altogether 6 different frequencies.
#define FREQ_CLASS_DENOMINATOR    171   //Constant to calculate the frequency from the AD value.

#define FALSE 0
#define TRUE  1

unsigned long time_stamp_32, time_stamp_500;      //Variables used for time stamps of tasks.
unsigned long period_32 = 32+16;    //Period of the 32ms task in ms plus 16ms initial delay.
                                    //Each 800 ms there would be a conflict otherwise. In this way NO conflict!
unsigned long period_500 = 500;     //Period of the 200ms task in ms.

byte period_values[NUM_OF_FREQUENCIES] = { 60, 30, 20, 15, 10, 6 };   //OFF periods between to consecutive ON periods.
byte LED_on = FALSE;    //A flag indicating whether the LED is turned ON.

byte periods = period_values[0];     //Initial value of the number of OFF periods.
byte period_counter = periods - 1;   //Initial value of the period counter. After startup a blink is made.



/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port, the LED's pin, system clock.
 * Description:         Initialization of the USB, of the LED's pin, of the timestamps of tasks and
 *                      displaying the initial message.
 */
void setup()
{
  Serial.begin(19200);              //Initializing the serial bus with a baud rate 19200 bits/s.
  
  pinMode(ORANGE_LED, OUTPUT);      //The LED's pin shall be an output pin.
  digitalWrite(ORANGE_LED, HIGH);   //Turning OFF the LED.

  analogReference(DEFAULT);     //Using the default (internal) voltage reference for AD-conversion.

  time_stamp_32 = millis();    //Taking the initial time stamps of the tasks.
  time_stamp_500 = millis();

  Serial.println("\n\nInfrared measurements started.");    //Displaying the initial message.
}



/*
 * Function prototype:  void Task_32ms(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    The LED's pin, global variables of the blink control.
 * Description:         The 32ms task.
 */
void Task_32ms(void)
{
  if (LED_on == TRUE)   //If the LED is ON...
  {
    LED_on = FALSE;   //...it shall be turned OFF.
    digitalWrite(ORANGE_LED,HIGH);    //Turning OFF the LED.
    period_counter = 1;   //Resetting the period counter to 1.
  }
  else
  {
    period_counter++;   //During the OFF-periods their counter is increased.
    
    if (period_counter >= periods)    //Is the counter bigger than or equal to the prescribed number?
    {
      LED_on = TRUE;    //If yes, the LED shall be turned ON.
      digitalWrite(ORANGE_LED,LOW);   //Turning ON the LED.
    }
  }
}



/*
 * Function prototype:  void Task_500ms(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    USB, global variables of the blink control, port A0.
 * Description:         The 500ms task.
 */
void Task_500ms(void)
{
  unsigned int adc_value = 0;
  byte idx;
  byte blink_freq;      //Variable to index the blinking frequency.
   
  for (idx = 1; idx <= NR_OF_SAMPLES; idx++)
  {
    delay(BASIC_DELAY_TIME);    //Some delay between measurements. Not obligatory.
    adc_value += analogRead(ANALOG_INPUT);     //Cumulating the converted values.
  }
  adc_value /= NR_OF_SAMPLES;   //Calculating the average value.

  blink_freq = adc_value / FREQ_CLASS_DENOMINATOR;    //Calculation of the frequency class. Result = 0, 1, ..., 5.
  periods = period_values[blink_freq];    //Taking the blinking parameter from its array.

  Serial.print("IR = ");          //For test purposes the result is displayed.
  Serial.println(adc_value,DEC);
}



/*
 * Function prototype:  void loop(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    System clock.
 * Description:         The SCHEDULER.
 */
void loop(void)
{ 
  /* Evaluating the clock value and calling the appropriate task(s) if conditions are met. */
  
  if (millis() - time_stamp_32 >= 32)
  { 
    period_32 = 32;   //The period of the 32 ms task shall be reset. It is only necessary hence at the
                      //beginning this variable has a bigger value to realize a time shift, to avoid
                      //conflicts betwwen the 2 tasks.
    time_stamp_32 = millis();   //Resetting the time stamp.
    Task_32ms();                //Calling the 32 ms task.
  }

  if (millis() - time_stamp_500 >= period_500)
  { 
    time_stamp_500 = millis();  //Resetting the time stamp.
    Task_500ms();               //Calling the 500 ms task.
  }
}


