/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW10_P2_Robot_car_rolling"                                   */
/*****************************************************************************/
/* Circuit: -                                                                */
/*****************************************************************************/
/* Device: INITIO robot car + motor shield                                   */
/*****************************************************************************/

/*
General description:

The program controls the robot car in a prescribed way.

*/

#include <AFMotor.h>                    //This inclusion is needed to use functions written for the robot car.
AF_DCMotor left(3, MOTOR34_1KHZ);       //Defining the car's left...
AF_DCMotor right(4, MOTOR34_1KHZ);      //...and right side.

#define NUMBER_OF_VALUES    11          //So many control commands are listed in the appropriate arrays.


/* The control principle is based on 3 pieces of data:
speed for the right side,
speed for the left side,
time duration.
  Negative speed means backward motion.
  Examples of speed settings:
( 220, 100) means slight drifting to the left (the right side is faster).
(-180, 180) means turning around to the right (without any other motion).
( 180,-180) means turning around to the left (without any other motion).
(   0,   0) means a complete stop.
*/
int R_values[NUMBER_OF_VALUES] =   { 0, 150, 180, 220,  200, -200,  200, -200, 200, 160, 0 };
int L_values[NUMBER_OF_VALUES] =   { 0, 150, 180, 220, -200,  200, -200,  200, 200, 160, 0 };
int phase_time[NUMBER_OF_VALUES] = { 1,   1,   1,   1,    2,    1,    1,    2,   1,   1, 1 };


/*
 * Function prototype:  void adjustMotor(int R_value, int L_value)
 * Parameter(s):        Speed values for the right and left motors.
 * Return:              -
 * Other interfaces:    Functions for the motor control shield.
 * Description:         The function sets the PWM values of the right and left motors.
 */
void adjustMotor(int R_value, int L_value)
{   
  if (R_value < 0)    //If the right side's value is negative...
  {
    right.setSpeed((byte)(-R_value));   //...the absolute value is determined but
    right.run(BACKWARD);                //the motor is switched to rotate backward.
  }
  else if (R_value > 0)   //If the right side's value is positive...
  {
    right.setSpeed((byte)(R_value));    //...the value is loaded and
    right.run(FORWARD);                 //the motor is switched to rotate forward.
  }
  else
  {
    right.setSpeed((byte)(R_value));    //If the right side's value is zero...
    right.run(RELEASE);                 //the motor is stopped.
  }


  if (L_value < 0)   //If the left side's value is negative...
  {
    left.setSpeed((byte)(-L_value));    //...the absolute value is determined but
    left.run(BACKWARD);                 //the motor is switched to rotate backward.
  }
  else if (L_value > 0)   //If the right side's value is positive...
  {
    left.setSpeed((byte)(L_value));     //...the value is loaded and
    left.run(FORWARD);
  }
  else
  {
    left.setSpeed((byte)(L_value));     //If the left side's value is zero...
    left.run(RELEASE);                  //the motor is stopped.
  }

}


/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Functions for the motor control shield.
 * Description:         The function sets the PWM values of the right and left motors as defined in the control array.
 */
void setup(void)
{
  byte idx;

  for (idx = 0; idx < NUMBER_OF_VALUES; idx++)
  {
    adjustMotor(R_values[idx], L_values[idx]);
    delay(1000*phase_time[idx]);
  }
}



void loop(void)
{  
}


