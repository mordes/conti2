/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW10_P1_Scheduler"                                           */
/*****************************************************************************/
/* Circuit: -                                                                */
/*****************************************************************************/
/* EDUCATION FROM QUARANTINE                                                 */
/*****************************************************************************/

/*
 * In this short and simple code a SCHEDULER is implemented. The basis of the scheduling is a CLOCK that
 * is monitored in the loop() function. 
 *    In this example there is a 2 second task, a 5 second task and a 10 second task. Study the messages
 * sent to the serial port how the tasks are performed! Note the behavior if the elapsed time is a common
 * multiple of the tasks, i.e. 10s, 20s, ... As two tasks cannot be performed at the same time an initial
 * phase shift can help a lot. Apply the commented initial values instead of the present ones! Study the
 * behavior again!
 */

#define FALSE 0
#define TRUE  1

unsigned long time_stamp_2, time_stamp_5, time_stamp_10;      //Variables used for time stamps of tasks.
unsigned long period_2 = 2000;    //Period of the 2s task in ms.
unsigned long period_5 = 5000;    //Period of the 5s task in ms.
//unsigned long period_5 = 5500;    //Period of the 5s task (in ms) plus 500ms initial delay.
unsigned long period_10 = 10000;  //Period of the 10s task in ms.
//unsigned long period_10 = 11000;  //Period of the 10s task (in ms) plus 1000ms initial delay.

unsigned int number_of_idle_dots = 0; //This variable is used to determine when a 'line feed' is sent to
                                      //the serial port.

/*
 * Function prototype:  void displayTime(unsigned long l_time)
 * Parameter(s):        A time stamp taken at task entries
 * Return:              -
 * Other interfaces:    -
 * Description:         The function displays the time stamp with which it is called. Resolution: 0.01s.
 */
void displayTime(unsigned long l_time)
{
  unsigned int fraction_part;
  unsigned long integer_part;

  fraction_part = (unsigned int)(l_time % 100);
  integer_part = l_time / 100;

  Serial.print("\n");
  Serial.print(integer_part,DEC);
  Serial.print(".");
  if (fraction_part < 10)   //For the decimal padding, e.g. 3.05. Here for the "0".
  {
    Serial.print("0");
  }
  Serial.print(fraction_part,DEC);  
}


/*
 * Function prototype:  void Task_2s(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         The 2ms task.
 */
void Task_2s(void)
{
  unsigned long time_elapsed;
  
  time_elapsed = millis()/10;   //LSB = 0.01s.
  displayTime(time_elapsed);
  Serial.print("    2s task performed.");    //Sending a message to indicate that the task was called.
}


/*
 * Function prototype:  void Task_5s(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         The 5ms task.
 */
void Task_5s(void)
{
  unsigned long time_elapsed;
  
  time_elapsed = millis()/10;      //LSB = 0.01s.
  displayTime(time_elapsed);
  Serial.print("    5s task performed.");    //Sending a message to indicate that the task was called.
}


/*
 * Function prototype:  void Task_10s(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         The 10ms task.
 */
void Task_10s(void)
{
  unsigned long time_elapsed;
  
  time_elapsed = millis()/10;      //LSB = 0.01s.
  displayTime(time_elapsed);
  Serial.print("   10s task performed.");    //Sending a message to indicate that the task was called.
}


/*
 * Function prototype:  void Idle_Task(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         The Idle Task of the scheduler.
 */
void Idle_Task(void)
{
  if(number_of_idle_dots == 0)    //When a 'line feed' is needed.
  {
    Serial.print("\n");
  }
 
  number_of_idle_dots++;   //Counting the dots ('.') printed as the result of the Idle Task.

  if(number_of_idle_dots == 80)   //Resetting the counter.
  {
    number_of_idle_dots = 0;
  }
  
  Serial.print(".");    //Printing the DOT as the result of the Idle Task.
  delay(20);    //This delay is only applied to reduce the speed of printing the DOTs.
}


/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port.
 * Description:         Sending the initial message to the serial port.
 */
void setup()
{
  Serial.begin(9600);
  Serial.print("\n\n\n\nTask manager started...");

  time_stamp_2 = millis();    //Taking the initial time stamps of the tasks.
  time_stamp_5 = millis();
  time_stamp_10 = millis();
}


/*
 * Function prototype:  void loop(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         Engine of a SCHEDULER.
 */
void loop()
{ 
  /* Evaluating the clock value and calling the appropriate task(s) if conditions are met. */
  if (millis() - time_stamp_2 >= 2000)
  { 
    time_stamp_2 = millis();
    Task_2s();
    number_of_idle_dots = 0;
  }

  if (millis() - time_stamp_5 >= period_5)
  { 
    period_5 = 5000;  //This row is necessary to fix the period of the 5s task (in ms).
                      //Note that the initial value can be another value.
    time_stamp_5 = millis();
    Task_5s();
    number_of_idle_dots = 0;
  }

  if (millis() - time_stamp_10 >= period_10)
  { 
    period_10 = 10000;  //This row is necessary to fix the period of the 10s task (in ms).
                        //Note that the initial value ucan be another value.
    time_stamp_10 = millis();
    Task_10s();
    number_of_idle_dots = 0;
  }

  Idle_Task();
}
