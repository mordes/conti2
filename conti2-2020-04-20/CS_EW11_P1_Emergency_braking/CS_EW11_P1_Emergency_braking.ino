/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW11_P1_Emergency_braking"                                   */
/*****************************************************************************/
/* Circuit: -                                                                */
/*****************************************************************************/
/* Device: INITIO robot car + motor shield                                   */
/*****************************************************************************/
/* Reference:                                                                */
/* https://learn.adafruit.com/adafruit-motor-shield/af-dcmotor-class         */
/*****************************************************************************/

/*
General description:

The program controls the robot car in a prescribed way.

*/

#include <AFMotor.h>                    //This inclusion is needed to use functions written for the robot car.
AF_DCMotor left(3, MOTOR34_1KHZ);       //Defining the car's left...
AF_DCMotor right(4, MOTOR34_1KHZ);      //...and right side.

#define FALSE 0
#define TRUE  1

#define ECHO      10    //Pin of Arduino that is to be connected to the ECHO pin of the sensor.
#define TRIGGER    9    //Pin of Arduino that is to be connected to the TRIG pin of the sensor.

#define TRIGGER_PULSE_WIDTH   10  //To launch ultrasonic wave pulses the TRIG pin of the sensor shall
                                  //get a pulse of at least 10 microseconds.

#define INITIALIZING_DELAY    10    //An arbitrary delay in ms to let the sensor wake up.

#define DISTANCE_DIVIDER         58   //A constant to get the result in centimeters.
#define MUTE_TIMEOUT            360   //Time-out value for a mute period.
//#define MUTE_TIMEOUT            560   //Time-out value for a mute period. For testing. To produce ERROR 1.
#define NO_ECHO_TIMEOUT         200   //Time-out value for the echo to arrive.
//#define NO_ECHO_TIMEOUT          40     //Time-out value for the echo to arrive. For testing. To produce ERROR 2.
#define RANGE_LIMIT_TIMEOUT    8700   //The maximum distance is defined to be 150 cm.

#define BRAKING_DISTANCE    40      //The distance, in cm, when braking shall be performed.
#define CLEARANCE           50      //The distance, in cm, when accelerating shall be performed.

/* Error types during the distance measurement. */
typedef enum {  OK = 0,                  //No error - OK.
                ECHO_PIN_LEVEL_HIGH,     //The signal level must be LOW before the echo arrives. If not it shall be indicated.
                NO_ECHO,                 //There is no echo - time-out has been reached.
                OUT_OF_RANGE             //Obstacle too far.
              } SENSOR_ERROR;


unsigned long time_stamp_200;     //Time stamp used for the 200 ms Task.
unsigned long period_200 = 200;   //The period of the 200 ms Task.

/* The most important variables for motor control. */
byte accelerating = FALSE;    //The flag indicating that accelerating is being performed..
byte braking = FALSE;         //The flag indicating that braking is being performed.
byte motor_speed = 0;         //The value of the motor speed.



/*
 * Function prototype:  void triggerUltraSound(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Output pin TRIGGER.
 * Description:         The function sets the trigger level high, waits 11 microseconds and after that
 *                      sets the trigger level low again.
 */
void triggerUltraSound(void)
{
  digitalWrite(TRIGGER,HIGH);
  delayMicroseconds(TRIGGER_PULSE_WIDTH);
  digitalWrite(TRIGGER,LOW);
}


/*
 * Function prototype:  byte phase_1(void)
 * Parameter(s):        -
 * Return:              A flag indicating whether there is abnormal behavior, i.e. the ECHO
 *                      signal's level gets HIGH too early.
 * Other interfaces:    ECHO pin
 * Description:         The function monitors the ECHO signal's level for a certain time.
 *                      If the ECHO level gets HIGH in this period the error is reported.
 */
byte phase_1(void)
{
  byte error = FALSE;   //First we assume that there will be no error.
  unsigned long timestamp = micros();   //Getting a time stamp.

  while (micros() - timestamp <= MUTE_TIMEOUT)    //Monitoring the signal level for a time determined by constant MUTE_TIMEOUT.
  {
    if (digitalRead(ECHO) == HIGH)    //If the signal level gets HIGH...
    {
      error = TRUE;                   //...there is an error.
      break;                          //Jumping out of the 'while' cycle.
    }
  }
  
  return error;
}


/*
 * Function prototype:  byte phase_2(void)
 * Parameter(s):        -
 * Return:              A flag indicating whether there is abnormal behavior, i.e. the ECHO
 *                      signal's rising edge does not arrive within a certain time.
 * Other interfaces:    ECHO pin
 * Description:         The function monitors the ECHO signal's level for a certain time.
 *                      If the ECHO level does not change from LOW to HIGH whithin a certain time period the error is reported.
 */
byte phase_2(void)
{
  byte error = TRUE;            //First we assume that there will be an error.
  unsigned long timestamp = micros();     //Getting a time stamp.

  while (micros() - timestamp <= NO_ECHO_TIMEOUT)    //Monitoring the signal level for a time determined by constant NO_ECHO_TIMEOUT.
  {
    if (digitalRead(ECHO) == HIGH)    //If the signal level gets HIGH...
    {
      error = FALSE;                  //...everything is fine.
      break;                          //Jumping out of the 'while' cycle.
    }
  }
  
  return error;
}


/*
 * Function prototype:  byte phase_3(unsigned long* ppulse_width)
 * Parameter(s):        A pointer to a variable of the ECHO pulse. Phisical unit: microseconds.
 *                      The ECHO pulse must not be longer than this time value.
 * Return:              A flag indicating whether there is abnormal behavior, i.e. the ECHO
 *                      signal's falling edge does not arrive within a certain time.
 * Other interfaces:    ECHO pin
 * Description:         The function monitors the ECHO signal's level for a certain time.
 *                      If the ECHO level does not change from HIGH to LOW whithin a certain time period the error is reported.
 *                      This means that there is no obstacle within the prescribed distance.
 */
byte phase_3(unsigned long* ppulse_width)
{
  byte error = TRUE;            //First we assume that there will be an error.
  unsigned long timestamp = micros();     //Getting a time stamp.

  while (micros() - timestamp <= RANGE_LIMIT_TIMEOUT)     //Monitoring the signal level for a time determined by constant RANGE_LIMIT_TIMEOUT.
  {
    if (digitalRead(ECHO) == LOW)     //If the signal level gets LOW...
    {
      error = FALSE;              //...everything is fine.
      *ppulse_width = micros() - timestamp;   //Calculating the length of the ECHO pulse.
      break;                      //Jumping out of the 'while' cycle.
    }
  }
  
  return error;
}


/*
 * Function prototype:  void measureEcho(unsigned long* ppw, SENSOR_ERROR* perr)
 * Parameter(s):        1) A pointer to a variable of the ECHO pulse. Phisical unit: microseconds.
 *                      2) A pointer to a variable containing the error code
 * Return:              -
 * Other interfaces:    -
 * Description:         The function calls the 3 phases belonging to the ECHO pulse.
 *                      If any of the 3 phases returns an error the process is aborted.
 *                      If everything is fine the length of the ECHO pulse is determined.
 */
void measureEcho(unsigned long* ppw, unsigned int* pdist, SENSOR_ERROR* perr)
{
  SENSOR_ERROR error_code = OK;     //First we assume that there will be no error.
  unsigned long pulse_width = RANGE_LIMIT_TIMEOUT;      //Initial and invalid value for the length of the ECHO pulse.

  if (phase_1() == TRUE)       //Monitoring the ECHO sognal level in PHASE1.
  {
    error_code = ECHO_PIN_LEVEL_HIGH;   //Indicating that there was an error, i.e. the ECHO level got HIGH too early.
  }
  else if (phase_2() == TRUE) //Monitoring the ECHO sognal level in PHASE2.
  {
    error_code = NO_ECHO;                //Indicating that there was an error, i.e. the ECHO pulse did not arrive in time.
  }
  else if (phase_3(&pulse_width) == TRUE)   //Monitoring the ECHO sognal level in PHASE3.
  {
    error_code = OUT_OF_RANGE;           //Indicating that there was an error, i.e. the ECHO pulse is too long.
  }
  else
  {}    //We love MISRA C...

  *perr = error_code;     //The error code is returned.
  *ppw = pulse_width;     //The measured (or the initial) pulse width is returned.
  *pdist = (unsigned int)((pulse_width + DISTANCE_DIVIDER/2) / DISTANCE_DIVIDER);
}




/*
 * Function prototype:  void adjustMotor(int R_value, int L_value)
 * Parameter(s):        Speed values for the right and left motors.
 * Return:              -
 * Other interfaces:    Functions for the motor control shield.
 * Description:         The function sets the PWM values of the right and left motors.
 */
void adjustMotor(int R_value, int L_value)
{   
  if (R_value < 0)    //If the right side's value is negative...
  {
    right.setSpeed((byte)(-R_value));   //...the absolute value is determined but
    right.run(FORWARD);                //the motor is switched to rotate backward.
  }
  else if (R_value > 0)   //If the right side's value is positive...
  {
    right.setSpeed((byte)(R_value));    //...the value is loaded and
    right.run(BACKWARD);                 //the motor is switched to rotate forward.
  }
  else
  {
    right.setSpeed((byte)(R_value));    //If the right side's value is zero...
    right.run(RELEASE);                 //the motor is stopped.
  }


  if (L_value < 0)   //If the left side's value is negative...
  {
    left.setSpeed((byte)(-L_value));    //...the absolute value is determined but
    left.run(FORWARD);                 //the motor is switched to rotate backward.
  }
  else if (L_value > 0)   //If the right side's value is positive...
  {
    left.setSpeed((byte)(L_value));     //...the value is loaded and
    left.run(BACKWARD);
  }
  else
  {
    left.setSpeed((byte)(L_value));     //If the left side's value is zero...
    left.run(RELEASE);                  //the motor is stopped.
  }

}


/*
 * Function prototype:  void displayData(unsigned int l_distance, SENSOR_ERROR l_error_code)
 * Parameter(s):        distance, error code
 * Return:              -
 * Other interfaces:    global variable of the motor speed
 * Description:         The function sends the parameters to the USB for testing possibilities.
 */
void displayData(unsigned int l_distance, SENSOR_ERROR l_error_code)
{
  Serial.print("d = ");
  Serial.print(l_distance,DEC);
  Serial.print(" cm  -  E: ");
  Serial.print(l_error_code,DEC);
  Serial.print("  -  w = ");
  Serial.println(motor_speed,DEC);
}


/*
 * Function prototype:  void distanceMeasurement(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    global variables of the motor control flags
 * Description:         Determining the motor control based on the distance measurement.
 */
void distanceMeasurement(void)
{
  unsigned long pulse_width;    //Variable for the pulse width.
  SENSOR_ERROR error_code;      //Variable for the error code.
  unsigned int distance;        //Variable for the distance, in cm.
  
  triggerUltraSound();      //Releasing the ultrasonic pulse sequence.
  measureEcho(&pulse_width,&distance,&error_code);    //Accepting the echo pulse and measuring its length.
  displayData(distance,error_code);      //Displaying the measurement's results. Only for testing purposes!

  /* Setting the control flags... */
  if ( (error_code == ECHO_PIN_LEVEL_HIGH) || (error_code == NO_ECHO) )   //If there is a measurement error...
  {
    accelerating = FALSE;     //...emergency braking shall be performed.
    braking = TRUE;
  }
  else if ( distance < BRAKING_DISTANCE )     //If an obstacle is too close...
  {
    accelerating = FALSE;   //...emergency braking shall be performed.
    braking = TRUE;
  }
  else if ( (distance > CLEARANCE ) && (accelerating == FALSE) && (braking == FALSE) )    //If there is clearance and
  {                                                                                   //a previous braking is over...
    accelerating = TRUE;    //...the car can accelerate.
    braking = FALSE;
  }
  
}


/*
 * Function prototype:  void speedControl(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    global variables of the motor control flags and the motor speed
 * Description:         Task based accelerating or braking of the car.
 */
void speedControl(void)
{
  if ( (accelerating == TRUE) && (braking == FALSE) )   //If the car shall be accelerated.
  {
    if (motor_speed == 0)     //If the car is not moving...
    {
      motor_speed = 110;    //First speed setting.
    }
    motor_speed += 10;    //Step-wise speed increasing.
    if (motor_speed >= 220)   //If the speed limit is reached...
    {
      braking = TRUE;     //...the other flag is also set. The two flags mean now free running.
    }
  }
  else if ( (accelerating == FALSE) && (braking == TRUE) )    //If the car shall be slowed down.
  {
    if (motor_speed > 60)     //If the speed is still high...
    {
      motor_speed -= 60;    //Step-wise braking.
    }
    
    if (motor_speed < 80)   //If the speed is low enough...
    {
      motor_speed = 0;    //...a sudden stop is already OK.
      braking = FALSE;    //The braking flag is cleared. The two flags mean now a complete stop.
    }
  }
  else { /* MISRA */}
   
  adjustMotor(motor_speed,motor_speed);  //Sending the contorl values to the motor.
  
}


/*
 * Function prototype:  void Task_200ms(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         The 200 ms Task.
 */
void Task_200ms(void)
{
  distanceMeasurement();    //Measuring the distance.
  speedControl();           //Forming the speed control.
}


/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port and pins.
 * Description:         Initializing operations.
 */
void setup()
{
  Serial.begin(19200);           //Initializing the serial bus with a baud rate 9600 bits/s.
  
  pinMode(ECHO, OUTPUT);
  pinMode(TRIGGER, OUTPUT);
  digitalWrite(ECHO,LOW);       //According to the manual the sensor's both pins are first
  digitalWrite(TRIGGER,LOW);    //initialized by putting low level voltage to them.

  delay(INITIALIZING_DELAY);    //An arbitrary delay.
  
  pinMode(ECHO, INPUT);         //After this the Arduino's pin dedicated to ECHO is set as INPUT.
}



/*
 * Function prototype:  void loop(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    -
 * Description:         Engine of the SCHEDULER.
 */
void loop()
{ 
  /* Evaluating the clock value and calling the appropriate task(s) if conditions are met. */
  if (millis() - time_stamp_200 >= 200)
  { 
    time_stamp_200 = millis();
    Task_200ms();   //Calling the 200 ms Task.
  }
}

