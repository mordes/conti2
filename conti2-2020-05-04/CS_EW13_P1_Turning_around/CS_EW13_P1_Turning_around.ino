/*****************************************************************************/
/* Automotive Software and Hardware Development in Practice II.              */
/*****************************************************************************/
/* The CONTINENTAL Subject - School Year: 2019/2020 - Semester: 2nd          */
/*****************************************************************************/
/* Microcontroller: ARDUINO UNO                                              */
/*****************************************************************************/
/* Program: "CS_EW13_P1_Turning_around"                                      */
/*****************************************************************************/
/* Circuit: -                                                                */
/*****************************************************************************/
/* Device: INITIO robot car + motor shield                                   */
/*****************************************************************************/
/* Reference:                                                                */
/* https://learn.adafruit.com/adafruit-motor-shield/af-dcmotor-class         */
/*****************************************************************************/
/* Description:                                                              */
/*                                                                           */
/* The robot car makes turning maneuvers. During these 'standing rotations'  */
/* it counts the rising edges of the wheel sensor. A maneuver is over if the */
/* number of rising edges reaches a certain value.                           */
/*****************************************************************************/

#include <AFMotor.h>    //Including the library of the motor control shield.

AF_DCMotor left(3, MOTOR34_1KHZ);     //Setting the parameters of the motor of the left side.
AF_DCMotor right(4, MOTOR34_1KHZ);    //Setting the parameters of the motor of the right side.

#define WHEEL_SENSOR        2     //Any signal wire of a wheel sensor can be connected to this pin.
                                  //Note that this pin must be 2 (never used by the shield) or
                                  //3 (can be used by the shield in certain cases. There are 2 external
                                  //interrupts at the Arduino Uno. Pin 2 or 3.
                                  
#define FALSE               0     //Boolean constant.
#define TRUE                1     //Boolean constant.

#define BLUE_LED           13     //External LED.

#define EDGE_NUMBERS_1     84     //Approximately a full turn around.
#define EDGE_NUMBERS_2     42     //Approximately a half turn.
#define EDGE_NUMBERS_3     21     //Approximately a quater turn.

volatile int edge_counter = 0;    //A variable used for counting the rising edges.


/*
 * Function prototype:  void ISR_countEdges(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Global variable 'edge_counter'.
 * Description:         This interrupt service routine (ISR) counts the rising edges of the wheel sensor.
 */
void ISR_countEdges(void)
{
  edge_counter++;
}


/*
 * Function prototype:  void LED_blinking(void)
 * Parameter(s):        number of periods
 * Return:              -
 * Other interfaces:    The LED's pin.
 * Description:         Blinking the LED 5 times.
 */
void LED_blinking(byte periods)
{
  byte idx;
  
  for (idx = 1; idx <= periods; idx++)
  {
    digitalWrite(BLUE_LED, LOW);
    delay(200);
    digitalWrite(BLUE_LED, HIGH);
    delay(200);
  }  
}


/*
 * Function prototype:  void motorControl(int motor_speed, byte left_direction, byte right_direction)
 * Parameter(s):        self-explanatory
 * Return:              -
 * Other interfaces:    The motor shield.
 * Description:         Setting the speed and direction of the motors of the car.
 */
void motorControl(int motor_speed, byte left_direction, byte right_direction)
{
  left.setSpeed(motor_speed);       //Setting the speed of the left wheel pair.
  right.setSpeed(motor_speed);      //Setting the speed of the right wheel pair.
  left.run(left_direction);    //Starting the left wheel pair.
  right.run(right_direction);   //Starting the right wheel pair.      
}


/*
 * Function prototype:  void setup(void)
 * Parameter(s):        -
 * Return:              -
 * Other interfaces:    Serial port, motor control shield.
 * Description:         Initialization of the USB, of the motor shield and attaching the interrupt
 *                      service routine to pin 2.
 */
void setup(void)
{
  byte idx;

  pinMode(BLUE_LED, OUTPUT); 
  
  left.run(RELEASE);              //Stopping the left wheel pair.
  right.run(RELEASE);             //Stopping the right wheel pair.
  
  attachInterrupt(digitalPinToInterrupt(WHEEL_SENSOR), ISR_countEdges, RISING);   //Attaching the ISR.
  //It means:   pin of the wheel sensor, i.e. pin 2;   function name;   interrupt triggered by rising edges.

  LED_blinking(20);     //A warning signal. Rotation is going to be performed.
  edge_counter = 0;   //Resetting the edge counter.
  motorControl(220, BACKWARD, FORWARD);   //Starting the rotation.
  while (edge_counter <= EDGE_NUMBERS_1) {}   //Waiting for the prescribed number of rising edges of the wheel sensor.
  motorControl(  0, RELEASE , RELEASE);   //Stopping the car.

  LED_blinking(5);
  edge_counter = 0;
  motorControl(220, BACKWARD, FORWARD);
  while (edge_counter <= EDGE_NUMBERS_2) {}
  motorControl(  0, RELEASE , RELEASE);

  LED_blinking(5);
  edge_counter = 0;
  motorControl(220, BACKWARD, FORWARD);
  while (edge_counter <= EDGE_NUMBERS_3) {}
  motorControl(  0, RELEASE , RELEASE);
}


void loop(void)
{
}

